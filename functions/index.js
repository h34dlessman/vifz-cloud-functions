const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp();

var AWS = require('aws-sdk');
//https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/s3-example-photo-album.html
AWS.config.region = 'us-east-1'; // Region
AWS.config.credentials = new AWS.CognitoIdentityCredentials({
  IdentityPoolId: 'us-east-1:ce16e520-bbfd-420c-b881-e72f3fa1eba3',
});
var bucket = "z.vifz.it"
var s3 = new AWS.S3({
  apiVersion: "2006-03-01",
  params: { Bucket: bucket }
});


exports.brewViFzURL = functions.https
.onCall((data, context) => {
  const id = data.id
  var storagePath = "https://firebasestorage.googleapis.com/v0/b/vifz-e52cc.appspot.com/o/vifz%2F"

  const vidLink = storagePath+id+".mp4?alt=media"
  //const gifLink = storagePath+id+"GiF?alt=media"

  // Create Link in AWS
  var objectParams = {
    Bucket: bucket,
    ACL: 'public-read',
    Key: id,
    Body: '',
    WebsiteRedirectLocation: vidLink
  };
  var uploadPromise = new AWS.S3({apiVersion: '2006-03-01'}).putObject(objectParams).promise();
  uploadPromise.then(
    function(data) {
      //console.log("Video Link Brewed");
    });


  });
